package lc.urbanfun;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lc on 8/22/14.
 * maintains a list of items that the user has done and rated - not sorted by activity type
 * displays name and number of stars in list
 */

public class ArchivedListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<List<FunThing>> {

    private static final String TAG = "ArchivedListFragment";
    private static final int LOADER_ID = 2;

    private Button mDeleteButton;
    private ArchivedAdapter mAdapter;

    public static ArchivedListFragment newInstance() {
        return new ArchivedListFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.archived_list_title);
        //might wanna move this so actionbar definitely created
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        mAdapter = new ArchivedAdapter(getActivity());
        setListAdapter(mAdapter);
        //loader will get archived item array to be shown on list
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEmptyText(getString(R.string.archived_list_empty));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<List<FunThing>> onCreateLoader(int id, Bundle args) {
        return new ArchiveLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<FunThing>> loader, List<FunThing> data) {
        if (data != null) {
            mAdapter.clear();
            mAdapter.addAll(data);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<FunThing>> loader) {
        //nothing pressing to do here at the moment
    }

    private static class ArchivedAdapter extends ArrayAdapter<FunThing> {

        private final Context mContext;

        public ArchivedAdapter(Context context) {
            super(context, 0);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.fragment_archived_list_item_view, parent, false);
                holder = new ViewHolder();
                holder.funName = (TextView) convertView.findViewById(R.id.archived_list_item_textview);
                holder.ratingBar = (RatingBar) convertView.findViewById(R.id.archived_list_item_ratings);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final FunThing funThing = getItem(position);

            /* have not included delete button yet
            mDeleteButton = (ImageButton) convertView.findViewById(R.id.archived_list_item_delete);
            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = mDatabaseManager.deleteById(FunDatabaseHelper.TABLE_FUN, funThing.get_id());
                    adapter.notifyDataSetChanged();
                }
            }); */
            holder.ratingBar.setIsIndicator(true);
            holder.ratingBar.setMax(5);
            LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(R.color.primary_dark_500, PorterDuff.Mode.SRC_ATOP);


            holder.ratingBar.setRating(funThing.getFunStars());
            holder.funName.setText(funThing.getFunThingName());
            return convertView;
        }
    }

    private static class ViewHolder {
        public TextView funName;
        public RatingBar ratingBar;
    }



}
