package lc.urbanfun;

import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RatingBar;

/**
 * Created by lc on 8/20/14.
 * not going to worry about async task in this one since load should be so quick
 * this fragment is brought up when user indicates that a particular list item in choiceslistfragment
 * has been completed - brings up a dialog box that allows user to rate the item and either
 * keep it in the original list or move it into the list of archived items (if moved to archived it
 * deletes item from main FunThingTable and adds it to ArchivedData
 * sends broadcast when finished if data has changed
 */
public class CompletedFragment extends DialogFragment {

    private static final String TAG = "CompletedFragment";
    private static final String COMPLETED_FUN_ID = "CompletedFunId";

    private DatabaseManager mDatabaseManager;
    private RatingBar mRatingBar;

    /**
     * @param funThingId indicates which specific item has been selected
     */
    public static CompletedFragment newInstance(long funThingId) {
        CompletedFragment completedFragment = new CompletedFragment();
        Bundle args = new Bundle();
        args.putLong(COMPLETED_FUN_ID, funThingId);
        completedFragment.setArguments(args);
        return completedFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final Long funId = getArguments().getLong(COMPLETED_FUN_ID);


        mDatabaseManager = DatabaseManager.get(getActivity());

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View completedView = inflater.inflate(R.layout.rating_dialog, container, false);
        mRatingBar = (RatingBar) completedView.findViewById(R.id.completed_ratingbar);
        LayerDrawable stars = (LayerDrawable) mRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(R.color.primary_med_300, PorterDuff.Mode.SRC_ATOP);
        Button archiveButton = (Button) completedView.findViewById(R.id.archive_button);
        archiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FunThing funThing = setStarsandGetFunThing(funId);
                if (funThing != null) {
                    Boolean success;
                    try {
                        mDatabaseManager.insertFunThing(FunDatabaseHelper.TABLE_ARCHIVE, funThing);
                        success = true;
                    } catch (Exception e) {
                        Log.d("Boo", "Archive database failed");
                        success = false;
                    }
                    if (success) {
                        mDatabaseManager.deleteById(FunDatabaseHelper.TABLE_FUN, funId);
                        sendUpdatedBroadcast();
                    }
                }
                dismiss();
            }
        });
        Button keepButton = (Button) completedView.findViewById(R.id.keep_button);
        keepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FunThing funThing = setStarsandGetFunThing(funId);
                if (funThing != null) {
                    ContentValues cv = new ContentValues();
                    cv.put(FunDatabaseHelper.COLUMN_FUN_STARS, (int) mRatingBar.getRating());
                    try {
                        mDatabaseManager.updateFunThing(cv, funId);
                    } catch (Exception e) {
                        Log.e("Boo", "Completed database not updated");
                    }
                }
                dismiss();
            }
        });

        return completedView;

    }

    private FunThing setStarsandGetFunThing(long funId) {
        int stars = (int) mRatingBar.getRating();
        DatabaseManager.FunCursor funCursor = mDatabaseManager.queryFunID(funId);
        FunThing funThing = null;
        if (funCursor.moveToFirst()) {
            funThing = funCursor.getFunThing();
            funCursor.close();
            funThing.setFunStars(stars);
        } else {
            Log.d("Boo", "Couldn't find fun thing");
        }
        return funThing;
    }

    private void sendUpdatedBroadcast() {
        Intent intent = new Intent(DatabaseManager.ACTION_DB_CHANGED);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        Log.d(TAG, "Broadcast Sent");
    }
}


