package lc.urbanfun;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lc on 8/2/14.
 * Class that contains all methods to access database
 * All calls to database should go through this class
 * One instance for entire app
 */

public class DatabaseManager {
    private static final String TAG = "DatabaseManager";
    public static final String ACTION_DB_CHANGED = "FUN_DATABASE_CHANGED";

    private static DatabaseManager sDatabaseManager;
    private final FunDatabaseHelper mHelper;
    private SQLiteDatabase mDb;
    private int mOpenCounter;

    private DatabaseManager(Context appContext) {
        mHelper = new FunDatabaseHelper(appContext);
    }

    public static DatabaseManager get(Context c) {
        if (sDatabaseManager == null) {
            sDatabaseManager = new DatabaseManager(c.getApplicationContext());
        }
        return sDatabaseManager;
    }


    SQLiteDatabase openDB() {
        mOpenCounter++;
        if (mOpenCounter == 1 && mDb == null) {
            mDb = mHelper.getWritableDatabase();
        }
        return mDb;
    }

/* originally implemented bc wanted to be sure to close database - then read that if really wasn't
that big of a deal so didn't bother - keeping in case change mind later...

    public void closeDB() {
        mOpenCounter--;
        if (mOpenCounter == 0) {
            mDb.close();
        }
    }
*/

    /**
     * inserts a new item into the database
     *
     * @param tableName tells which table to insert item into
     * @param funthing  gives it the item to put in table
     * @return id of inserted item
     */
    public long insertFunThing(String tableName, FunThing funthing) {
        ContentValues cv = new ContentValues();
        cv.put(FunDatabaseHelper.COLUMN_FUN_TYPE, funthing.getFunThingType().name());
        cv.put(FunDatabaseHelper.COLUMN_FUN_NAME, funthing.getFunThingName());
        cv.put(FunDatabaseHelper.COLUMN_FUN_LOCATION, funthing.getFunThingLocation());
        cv.put(FunDatabaseHelper.COLUMN_FUN_PHONE_NUMBER, funthing.getFunThingPhoneNumber());
        cv.put(FunDatabaseHelper.COLUMN_FUN_DATE, funthing.getFunThingDate());
        cv.put(FunDatabaseHelper.COLUMN_FUN_URI, funthing.getFunThingURI());
        cv.put(FunDatabaseHelper.COLUMN_FUN_PHOTO, funthing.getFunThingPhoto());
        cv.put(FunDatabaseHelper.COLUMN_FUN_STARS, funthing.getFunStars());
        return openDB().insert(tableName, null, cv);
    }

    public int updateFunThing(ContentValues cv, long id) {
        return openDB().update(FunDatabaseHelper.TABLE_FUN, cv, FunDatabaseHelper.COLUMN_FUN_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public int deleteById(String tableName, long id) {

        return openDB().delete(tableName, FunDatabaseHelper.COLUMN_FUN_ID + " = ?", new String[]{String.valueOf(id)});
    }

    //queries database for one object that matches unique id number

    /**
     * retrieves one item with a particular id number
     *
     * @param id indicates which item to fetch
     * @return a wrapped cursor
     */
    public FunCursor queryFunID(long id) {
        Cursor wrapped = openDB().query(FunDatabaseHelper.TABLE_FUN, null,
                FunDatabaseHelper.COLUMN_FUN_ID + " = ?",
                new String[]{String.valueOf(id)},
                null, null, null, "1");
        return new FunCursor(wrapped);
    }

    /**
     * treating this one somewhat differently bc don't break it up by category
     * returns entire archived list of objects
     */
    public List<FunThing> getArchivedList() {
        List<FunThing> archiveList = new ArrayList<FunThing>();
        Cursor cursor = openDB().query(FunDatabaseHelper.TABLE_ARCHIVE, null, null, null, null, null, null);
        if (cursor != null) {
            FunCursor funCursor = new FunCursor(cursor);
            funCursor.moveToFirst();
            while (!funCursor.isAfterLast()) {
                FunThing funThing = funCursor.getFunThing();
                archiveList.add(funThing);
                cursor.moveToNext();
            }
        }
        if (cursor != null) cursor.close();
        return archiveList;
    }

    //queries database for list of objects of a specific category (bar, restaurant, etc.)

    /**
     * gets all items within a particular category
     *
     * @param tableName tells it which table to query
     * @param funType   tells it which category to get the results for
     * @return a wrapped cursor
     */
    public FunCursor queryFunType(String tableName, FunType funType) {
        Cursor wrapped = openDB().query(tableName, null,
                FunDatabaseHelper.COLUMN_FUN_TYPE + " = ?",
                new String[]{funType.name()},
                null, null, null);
        return new FunCursor(wrapped);
    }

    public static class FunCursor extends CursorWrapper {

        public FunCursor(Cursor c) {
            super(c);
        }

        public FunThing getFunThing() {
            //put in a move to first here?  had a problem with it in update funthing
            if (isBeforeFirst() || isAfterLast()) return null;

            FunThing funThing = new FunThing();
            long funId = getLong(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_ID));
            funThing.set_id(funId);
            String funType = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_TYPE));
            funThing.setFunThingType(FunType.valueOf(funType));
            String funName = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_NAME));
            funThing.setFunThingName(funName);
            String funLocation = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_LOCATION));
            funThing.setFunThingLocation(funLocation);
            String funPhone = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_PHONE_NUMBER));
            funThing.setFunThingPhoneNumber(funPhone);
            String funDate = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_DATE));
            funThing.setFunThingDate(funDate);
            String funUri = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_URI));
            funThing.setFunThingURI(funUri);
            String funPhoto = getString(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_PHOTO));
            funThing.setFunThingPhoto(funPhoto);
            int funStars = getInt(getColumnIndex(FunDatabaseHelper.COLUMN_FUN_STARS));
            funThing.setFunStars(funStars);
            return funThing;

        }


    }

}
