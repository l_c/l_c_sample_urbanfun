package lc.urbanfun;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by lc on 7/30/14.
 * called from main urbanfunfragment to display random even chosen
 * depending on what info is available for a given result, allows user to call or
 * go to the website of the selected item
 */

public class ResultsFragment extends DialogFragment implements PhotoFinishedListener {

    private static final String TAG = "ResultsFragment";
    private static final String RESULT_ID = "ResultID";
    private static final String RESULT_TYPE = "ResultType";

    private String mPhone;
    private int mView;
    private FunThing mFunResult;
    private FunType mType;
    private String mWebsite;
    private ImageView mPhoto;


    public static ResultsFragment newInstance(FunType type, long fun_id) {
        ResultsFragment resultsFragment = new ResultsFragment();
        Bundle args = new Bundle();
        args.putSerializable(RESULT_TYPE, type);
        args.putLong(RESULT_ID, fun_id);
        resultsFragment.setArguments(args);
        return resultsFragment;
    }

    /**
     * gets category type and id of chosen item from args and chooses layout based on
     * category
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        final Bundle args = getArguments();
        mType = (FunType) args.getSerializable(RESULT_TYPE);
        long id = args.getLong(RESULT_ID);
        if (id != -1) {
            mFunResult = getFunThing(id);
            switch (mType) {
                case RESTAURANT:
                    mView = R.layout.results_dialog_2button;
                    break;
                case COOK:
                case EVENT:
                case BAR:
                    mView = R.layout.results_dialog_1button;
                    break;
                case ACTIVITY:
                case NIGHTLIFE:
                    mView = R.layout.results_dialog_photo;
                    break;
                default:
                    mView = R.layout.results_dialog_photo;
                    break;
            }
        } else {
            mView = R.layout.results_dialog_photo;
        }
    }

    /**
     * populates layout fields for given selection, starts method to retrieve photo from web if needed,
     * and disables buttons if information to use them is not available
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(mView, container, false);
        TextView tv = (TextView) v.findViewById(R.id.result_text);
        mPhoto = (ImageView) v.findViewById(R.id.result_image);

        if (mFunResult != null) {
            PhotoFetcher.setPhoto(mFunResult, this);
            String name = mFunResult.getFunThingName();
            if (name != null) {
                tv.setText(name);
            }
        } else {
            mPhoto.setImageResource(R.drawable.empty);
            tv.setText(R.string.no_entries + mType.name());
        }


        switch (mView) {
            case R.layout.results_dialog_2button:
                Button pButton = (Button) v.findViewById(R.id.result_phone);
                Button rButton = (Button) v.findViewById(R.id.result_reserve);
                mWebsite = mFunResult.getFunThingURI();
                if (mWebsite == null) {
                    rButton.setEnabled(false);

                } else if (mWebsite.length() == 0) {
                    rButton.setEnabled(false);

                } else {
                    rButton.setOnClickListener(new WebListener());
                }

                mPhone = mFunResult.getFunThingPhoneNumber();
                if (mPhone == null) {
                    pButton.setEnabled(false);

                } else if (mPhone.length() == 0) {
                    pButton.setEnabled(false);
                } else {
                    pButton.setOnClickListener(new CallListener());
                }
                break;
            case R.layout.results_dialog_1button:
                Button button = (Button) v.findViewById(R.id.result_only_button);
                switch (mType) {
                    case BAR:
                        button.setText(R.string.call);
                        mPhone = mFunResult.getFunThingPhoneNumber();
                        if (mPhone == null || mPhone.equals("")) {
                            button.setEnabled(false);

                        } else if (mPhone.length() == 0) {
                            button.setEnabled(false);
                        } else {
                            button.setOnClickListener(new CallListener());
                        }
                        break;
                    case COOK:
                    case EVENT:
                        mWebsite = mFunResult.getFunThingURI();
                        if (mWebsite == null || mWebsite.equals("")) {
                            button.setEnabled(false);

                        } else if (mWebsite.length() == 0) {
                            button.setEnabled(false);
                        } else {
                            button.setOnClickListener(new WebListener());
                        }
                        break;
                }
                break;
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        getDialog().getWindow().setLayout(width - 100, height / 2);
    }

    @Override
    public void onPhotoFinished(Bitmap bitmap) {
        mPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void setPhotoResource(int photoId) {
        mPhoto.setImageResource(photoId);
    }


    private FunThing getFunThing(long id) {
        DatabaseManager databaseManager = DatabaseManager.get(getActivity());
        DatabaseManager.FunCursor funCursor = databaseManager.queryFunID(id);
        FunThing funResult = new FunThing();

        //the moveToFirst check should obviate the need to null check funResult later
        if (funCursor.moveToFirst()) {
            funResult = funCursor.getFunThing();
        } else {
            Toast.makeText(getActivity(),R.string.no_result_toast, Toast.LENGTH_LONG).show();
            dismiss();
        }
        return funResult;
    }


    private class CallListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            try {
                String toDial = "tel:" + mPhone;
                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse(toDial));
                startActivity(i);
            } catch (Exception e) {
                Toast.makeText(getActivity(),R.string.call_failed_toast, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class WebListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (FunApplication.isDataConnectionAvailable()) {
                try {
                    if (!mWebsite.startsWith("https://") && !mWebsite.startsWith("http://")) {
                        mWebsite = "http://" + mWebsite;
                    }
                    Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebsite));
                    startActivity(openUrlIntent);
                } catch (Exception e) {
                    Toast.makeText(getActivity(),R.string.website_unavailable_toast, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getActivity(),R.string.no_internet_toast, Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }

}
