package lc.urbanfun;

/**
 * Created by lc on 8/1/14.
 */
public class FunThing {

    private Long m_id;
    private FunType mFunThingType;
    private String mFunThingName;
    private String mFunThingLocation;
    private String mFunThingDate;
    private String mFunThingURI;
    private String mFunThingPhoto;
    private String mFunThingPhoneNumber;
    private int mFunStars;

    /* Originally implemented parcelable in here, but then decided really had no reason to pass the actual objects
    back and forth, so deleted all that code.
     */


    public FunThing() {
        m_id = -1L;
    }


    //overriding both equals and hashcode because don't want redundant things in list and all fields might not match

    /**
     * returns equal if name and category match
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof FunThing))
            return false;
        if (this == o)
            return true;
        if (((FunThing) o).mFunThingName.equals(this.mFunThingName) && ((FunThing) o).mFunThingType.equals(this.mFunThingType))
            return true;
        //if none of these conditions apply
        return false;

    }

    @Override
    public int hashCode() {
        if (mFunThingName != null) {
            return 17 * mFunThingName.hashCode();
        } else {
            return super.hashCode();
        }
    }

    public Long get_id() {
        return m_id;
    }

    public void set_id(Long _id) {
        m_id = _id;
    }

    public FunType getFunThingType() {
        return mFunThingType;
    }

    public void setFunThingType(FunType funThingType) {
        mFunThingType = funThingType;
    }

    public String getFunThingName() {
        return mFunThingName;
    }

    public void setFunThingName(String funThingName) {
        mFunThingName = funThingName;
    }

    public String getFunThingLocation() {
        return mFunThingLocation;
    }

    public void setFunThingLocation(String funThingLocation) {
        mFunThingLocation = funThingLocation;
    }

    public String getFunThingDate() {
        return mFunThingDate;
    }

    public void setFunThingDate(String funThingDate) {
        mFunThingDate = funThingDate;
    }

    public String getFunThingURI() {
        return mFunThingURI;
    }

    public void setFunThingURI(String funThingURI) {
        mFunThingURI = funThingURI;
    }

    public String getFunThingPhoto() {
        return mFunThingPhoto;
    }

    public void setFunThingPhoto(String funThingPhoto) {
        mFunThingPhoto = funThingPhoto;
    }

    public String getFunThingPhoneNumber() {
        return mFunThingPhoneNumber;
    }

    public void setFunThingPhoneNumber(String funThingPhoneNumber) {
        mFunThingPhoneNumber = funThingPhoneNumber;
    }

    public int getFunStars() {
        return mFunStars;
    }

    public void setFunStars(int funStars) {
        mFunStars = funStars;
    }

    @Override
    public String toString() {
        return mFunThingName;
    }
}
