package lc.urbanfun;

import android.content.AsyncTaskLoader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;

import lc.urbanfun.DatabaseManager;
import lc.urbanfun.FunThing;

/**
 * Created by lc on 9/13/14.
 * Loads list of archived items from database
 */
public class ArchiveLoader extends AsyncTaskLoader<List<FunThing>> {

    private static final String TAG = "ArchiveLoader";

    private static Context mAppContext;
    private static List<FunThing> mArchiveList;

    public ArchiveLoader(Context context) {
        super(context);
        mAppContext = context.getApplicationContext();
    }

    @Override
    public List<FunThing> loadInBackground() {
        DatabaseManager databaseManager = DatabaseManager.get(mAppContext);
        //for now sticking with db manager returning whole list for archived items
        List<FunThing> funArchiveList = databaseManager.getArchivedList();
        if (funArchiveList == null) {
            Log.e(TAG, "Didn't find any archived items in database");
        }
        return funArchiveList;
    }

    @Override
    public void deliverResult(List<FunThing> data) {
        //since not using cursor or anything at the moment, not protecting old data before delivering new
        if (isReset()) {
            return;
        }

        mArchiveList = data;

        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        LocalBroadcastManager.getInstance(mAppContext)
                .registerReceiver(dbReceiver, new IntentFilter(DatabaseManager.ACTION_DB_CHANGED));

        if (mArchiveList != null) {
            deliverResult(mArchiveList);
        }
        if (takeContentChanged() || mArchiveList == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        //ensure loading has stopped
        onStopLoading();
        if (mArchiveList != null) mArchiveList = null;
        //left receiver for now
    }

    private final BroadcastReceiver dbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onContentChanged();
            Log.d(TAG, "received broadcast");
        }
    };

}