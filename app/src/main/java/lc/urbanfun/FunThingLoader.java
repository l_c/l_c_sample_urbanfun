package lc.urbanfun;


import android.content.AsyncTaskLoader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import lc.urbanfun.DatabaseManager;
import lc.urbanfun.FunDatabaseHelper;
import lc.urbanfun.FunThing;
import lc.urbanfun.FunType;

/**
 * Created by lc on 8/26/14.
 * this loader goes to the database and gets the lists of potential items for each category and
 * returns it in a hashmap with category type as key pointing to list of items in that category
 */
public class FunThingLoader extends AsyncTaskLoader<Map<FunType, List<FunThing>>> {

    private static final String TAG = "FunThingLoader";

    private static Context mAppContext;
    private static Map<FunType, List<FunThing>> mFunMap;

    public FunThingLoader(Context context) {
        super(context);
        mAppContext = context.getApplicationContext();


    }

    @Override
    public Map<FunType, List<FunThing>> loadInBackground() {
        Map<FunType, List<FunThing>> funThingMap = new EnumMap<FunType, List<FunThing>>(FunType.class);
        for (FunType type : FunType.values()) {
            List<FunThing> funThingList = new ArrayList<FunThing>();
            DatabaseManager databaseManager = DatabaseManager.get(mAppContext);
            DatabaseManager.FunCursor funCursor = databaseManager.queryFunType(FunDatabaseHelper.TABLE_FUN, type);
            if (funCursor.moveToFirst()) {
                while (!funCursor.isAfterLast()) {
                    funThingList.add(funCursor.getFunThing());
                    funCursor.moveToNext();
                }
            } else {
                Log.d(TAG, "Didn't find any" + type + "results in database");
                //fun thing list will be null
            }
            funCursor.close();
            funThingMap.put(type, funThingList);
        }

        return funThingMap;
    }

    @Override
    public void deliverResult(Map<FunType, List<FunThing>> data) {
        //since not using cursor or anything at the moment, not protecting old data before delivering new
        if (isReset()) {
            return;
        }

        mFunMap = data;

        if (isStarted())
            super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {

        LocalBroadcastManager.getInstance(mAppContext)
                .registerReceiver(dbReceiver, new IntentFilter(DatabaseManager.ACTION_DB_CHANGED));

        if (mFunMap != null) {
            deliverResult(mFunMap);
        }
        if (takeContentChanged() || mFunMap == null) {
            forceLoad();
        }
    }


    @Override
    protected void onStopLoading() {
        //attempt to cancel the current load task if possible
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        //ensure loading has stopped
        onStopLoading();
        if (mFunMap != null) mFunMap = null;
        //keeping receiver for now
    }

    private final BroadcastReceiver dbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onContentChanged();
        }
    };
}
