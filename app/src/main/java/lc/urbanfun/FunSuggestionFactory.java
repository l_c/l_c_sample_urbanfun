package lc.urbanfun;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * Created by lc on 8/11/14.
 * singleton holder of suggestion lists
 * gets suggestions for each category from the database and keeps the results in a hashmap
 * with key of category type
 * at this point pull from database at the beginning of an app session and if changes are made update
 * the list directly as well as changing suggestiontable in database
 * FunSuggestionFactory will keep same list references the same time and the lists will not be
 * repopulated from database until it is destroyed and recreated
 */
public class FunSuggestionFactory {

    private static final String TAG = "FunSuggestionFactory";
    private static FunSuggestionFactory mFunSuggestionFactory;
    private static Context mAppContext;
    private final List<FunThing> mFunBars;
    private final List<FunThing> mFunRestaurants;
    private final List<FunThing> mFunActivities;
    private final List<FunThing> mFunEvents;
    private final List<FunThing> mFunCook;
    private final List<FunThing> mFunNightlife;

    private final EnumMap<FunType, List<FunThing>> mCategoryMap = new EnumMap<FunType, List<FunThing>>(FunType.class);

    private FunSuggestionFactory(Context appContext) {
        mAppContext = appContext;
        mFunBars = getFunSuggestions(FunType.BAR);
        Log.e(TAG,mFunBars.toString());
        mFunRestaurants = getFunSuggestions(FunType.RESTAURANT);
        mFunActivities = getFunSuggestions(FunType.ACTIVITY);
        mFunEvents = getFunSuggestions(FunType.EVENT);
        mFunNightlife = getFunSuggestions(FunType.NIGHTLIFE);
        mFunCook = getFunSuggestions(FunType.COOK);
        mCategoryMap.put(FunType.ACTIVITY, mFunActivities);
        mCategoryMap.put(FunType.BAR, mFunBars);
        mCategoryMap.put(FunType.COOK, mFunCook);
        mCategoryMap.put(FunType.NIGHTLIFE, mFunNightlife);
        mCategoryMap.put(FunType.RESTAURANT, mFunRestaurants);
        mCategoryMap.put(FunType.EVENT, mFunEvents);
    }

    public static FunSuggestionFactory get(Context c) {
        if (mFunSuggestionFactory == null) {
            mFunSuggestionFactory = new FunSuggestionFactory(c.getApplicationContext());
        }
        return mFunSuggestionFactory;
    }

    private static List<FunThing> getFunSuggestions(FunType type) {
        List<FunThing> funList = new ArrayList<FunThing>();
        DatabaseManager databaseManager = DatabaseManager.get(mAppContext);
        DatabaseManager.FunCursor funCursor = databaseManager.queryFunType(FunDatabaseHelper.TABLE_SUGGESTIONS, type);
        if (funCursor.moveToFirst()) {
            while (!funCursor.isAfterLast()) {
                funList.add(funCursor.getFunThing());
                funCursor.moveToNext();
            }
        } else {
            Log.e(TAG, "Didn't find any" + type.name() + "results in database");
        }
        funCursor.close();
        return funList;
    }

    public List<FunThing> getFunThingSuggestionList(FunType type) {
        return mCategoryMap.get(type);
    }

    public void deleteSuggestionFromList(FunType type, FunThing funThing) {
        switch (type) {
            case BAR:
                mFunBars.remove(funThing);
                break;
            case RESTAURANT:
                mFunRestaurants.remove(funThing);
                break;
            case ACTIVITY:
                mFunActivities.remove(funThing);
                break;
            case NIGHTLIFE:
                mFunNightlife.remove(funThing);
                break;
            case COOK:
                mFunCook.remove(funThing);
                break;
            case EVENT:
                mFunEvents.remove(funThing);
                break;
            default:
                Log.e(TAG, "Something is wrong with FunType Enum definition");
                break;
        }
    }

    public void addSuggestionToList(FunType type, FunThing funThing) {
        switch (type) {
            case BAR:
                mFunBars.add(funThing);
                break;
            case RESTAURANT:
                mFunRestaurants.add(funThing);
                break;
            case ACTIVITY:
                mFunActivities.add(funThing);
                break;
            case NIGHTLIFE:
                mFunNightlife.add(funThing);
                break;
            case COOK:
                mFunCook.add(funThing);
                break;
            case EVENT:
                mFunEvents.add(funThing);
                break;
            default:
                Log.e(TAG, "Something is wrong with FunType Enum definition");
                break;
        }
    }

}

