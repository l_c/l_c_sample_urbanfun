package lc.urbanfun;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by lc on 8/1/14.
 * currently called either from add item menu item to create a new item or
 * choiceslistfragment on item click to display/update info on an already existing item
 */
public class FunThingDetailFragment extends Fragment implements PhotoFinishedListener {

    public static final String FUN_ID = "FunThingId";
    public static final String PAGER_POSITION = "Position_of_ViewPager_when_Selection_Made";
    public static final String DATA_CHANGED = "Was_data_changed_in_this_instantiation";
    private static final String TAG = "FunThingDetailFragment";

    private DatabaseManager mDatabaseManager;
    private FunThing mFunThing;
    private int mPagerPosition;
    private boolean isNew;
    private boolean mDataAdded;

    private ProgressBar mProgressBar;
    private Spinner mFunTypeSpinner;
    private EditText mFunPhone, mFunWebsite, mFunName;
    private ImageView mFunPhoto;


    public static FunThingDetailFragment newInstance(long id, int pagerPosition, boolean dataChanged) {
        Bundle args = new Bundle();
        args.putLong(FUN_ID, id);
        args.putInt(PAGER_POSITION, pagerPosition);
        args.putBoolean(DATA_CHANGED, dataChanged);
        FunThingDetailFragment fragment = new FunThingDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        long id = getArguments().getLong(FUN_ID);
        mPagerPosition = getArguments().getInt(PAGER_POSITION);
        mDataAdded = getArguments().getBoolean(DATA_CHANGED);
        mDatabaseManager = DatabaseManager.get(getActivity());

        if (id == -1) {
            //create new FunThing
            isNew = true;
            mFunThing = new FunThing();
        } else {
            //viewing/editing an already existing item, which need to fetch
            isNew = false;
            DatabaseManager.FunCursor funCursor = mDatabaseManager.queryFunID(id);
            if (funCursor.moveToFirst()) {
                mFunThing = funCursor.getFunThing();
            }
        }
    }

    /**
     * if showing previously created item, sets spinner to current category and populates
     * data fields with current info
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_fun_thing_detail, container, false);
        //might wanna move this to where actionbar definitely has been created
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        mFunTypeSpinner = (Spinner) v.findViewById(R.id.funThingType);
        ArrayAdapter<FunType> dataAdapter = new ArrayAdapter<FunType>(getActivity(), android.R.layout.simple_spinner_item, FunType.values());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFunTypeSpinner.setAdapter(dataAdapter);
        if (mPagerPosition != 0) {
            mFunTypeSpinner.post(new Runnable() {
                @Override
                public void run() {
                    mFunTypeSpinner.setSelection(mPagerPosition, true);
                }
            });
        }
        mFunTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFunThing.setFunThingType(FunType.values()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //nothing needed right now
            }
        });

        mFunPhone = (EditText) v.findViewById(R.id.funThingPhoneNumber);
        String number = mFunThing.getFunThingPhoneNumber();
        if (number != null) mFunPhone.setText(number);
        mProgressBar = (ProgressBar) v.findViewById(R.id.detail_image_progress);
        mFunPhoto = (ImageView) v.findViewById(R.id.funThingPhoto);
        if (!isNew) {
            mProgressBar.setVisibility(View.VISIBLE);
            //photofetcher finds the photoresource for the item sent in
            PhotoFetcher.setPhoto(mFunThing, this);
        }

        /* testing different ways of loading photos - in real app would have to determine source and load differently
        right now only have drawables for prepopulated items and web for some other cases - and only thing that is stored
        for an item in its photo field is a string for web - fetching from web with async task
         for now - not necessarily the best solution, but wanted to make sure had tried an async task - should
         be okay for just one picture bc short running task*/

        //create separate variables for inputs for now for easier debugging
        mFunWebsite = (EditText) v.findViewById(R.id.funThingURL);
        String website = mFunThing.getFunThingURI();
        if (website != null) mFunWebsite.setText(website);
        mFunName = (EditText) v.findViewById(R.id.funThingName);
        String name = mFunThing.getFunThingName();
        if (name != null) mFunName.setText(name);
        //save button saves item and closes fragment
        Button saveButton = (Button) v.findViewById(R.id.button_funThingSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFunName.getText().length() != 0) {
                    updateItems();
                    getActivity().finish();
                } else {
                    Toast.makeText(getActivity(), "New item without a name cannot be saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //add button saves current item and brings up another detail view to add an additional item
        Button addButton = (Button) v.findViewById(R.id.button_funThingAddAnotherSave);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFunName.getText().length() != 0) {
                    updateItems();
                    FunThingDetailFragment newFragment = newInstance(-1L, mPagerPosition, mDataAdded);
                    FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragmentContainer, newFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Toast.makeText(getActivity(), "New item without a name cannot be saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //cancel leaves fragment without saving/changing data on the current entry
        Button cancelButton = (Button) v.findViewById(R.id.button_funThingDismiss);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


        return v;

    }

    /**
     * sends broadcast if data used by other fragments was changed within this fragment
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mDataAdded) {
            Intent intent = new Intent(DatabaseManager.ACTION_DB_CHANGED);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            Log.d(TAG, "Broadcast Sent");
        }
    }

    private void updateItems() {
        if (isNew) {
            insertNewItem();
        } else {
            updateExistingItem();
        }
        mDataAdded = true;
    }

    private void insertNewItem() {
        mFunThing.setFunThingName(mFunName.getText().toString());
        mFunThing.setFunThingURI(mFunWebsite.getText().toString());
        mFunThing.setFunThingPhoneNumber(mFunPhone.getText().toString());
        //insertFunThing creates new entry in database - id will be added upon insert and then set
        // to the item when it is first fetched from the database
        mDatabaseManager.insertFunThing(FunDatabaseHelper.TABLE_FUN, mFunThing);
    }

    private void updateExistingItem() {
        ContentValues cv = new ContentValues();
        cv.put(FunDatabaseHelper.COLUMN_FUN_NAME, mFunName.getText().toString());
        cv.put(FunDatabaseHelper.COLUMN_FUN_URI, mFunWebsite.getText().toString());
        cv.put(FunDatabaseHelper.COLUMN_FUN_PHONE_NUMBER, mFunPhone.getText().toString());
        //updateFunThing updates existing entry in database
        mDatabaseManager.updateFunThing(cv, mFunThing.get_id());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * called if PhotoFetcher.setPhoto finds a bitmap as the photo for the given item
     */
    @Override
    public void onPhotoFinished(Bitmap bitmap) {
        mProgressBar.setVisibility(View.INVISIBLE);
        if (bitmap != null) {
            mFunPhoto.setImageBitmap(bitmap);
        } else {
            mFunPhoto.setImageResource(mFunThing.getFunThingType().getPhotoId());
        }
    }

    /**
     * called if PhotoFetcher.setPhoto finds a resource id as the photo for the given item
     */
    @Override
    public void setPhotoResource(int photoId) {
        mProgressBar.setVisibility(View.INVISIBLE);
        mFunPhoto.setImageResource(photoId);
    }

}
