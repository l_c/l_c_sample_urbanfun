package lc.urbanfun;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.signature.HmacSha1MessageSigner;
import oauth.signpost.signature.QueryStringSigningStrategy;

/**
 * Created by lc on 8/8/14.
 * <p/>
 * gets current suggestion list from singleton FunSuggestionFactory (keeping the singleton suggestion
 * factory implementation rather than directly updating and reloading from database to update ui for
 * a bit because only use it in one activity)
 * then brings up current items in suggestion database, loads more from yelp if requested
 * and allows the user to transfer items from the suggestion list into their primary list
 */
public class SuggestionFragment extends Fragment {

    private static final String TAG = "SuggestionFragment";

    private static final String YELP_CONSUMER_KEY = "oYG7buUxt0pe1DnIKttmew";
    private static final String YELP_CONSUMER_SECRET = "UrYDxEORsdJxnGjCh2P5Jn-VGPw";
    private static final String YELP_TOKEN = "cDJ9VBSazvPKD8gsUvNDePR8FPwSRD_k";
    private static final String YELP_TOKEN_SECRET = "1-Pc2mW8coXlyOJ0hNASN7WgpRI";
    private static final String YELP_ACTIVITY = "active";
    private static final String YELP_ENTERTAINMENT = "arts";
    private static final String YELP_EVENT = "yelpevents";
    private static final String YELP_RESTAURANT = "restaurants";
    private static final String YELP_BARS = "bars";
    private static final String YELP_NIGHTLIFE = "danceclubs";

    private FunType mType;
    private DatabaseManager mDatabaseManager;
    private ArrayAdapter<FunThing> mArrayAdapter;
    private List<FunThing> mFunList;
    private Button mLoadMoreButton;
    private List<FunThing> mSuggestionSelected;
    private FunSuggestionFactory mSuggestionFactory;
    private int mYelpHits;
    private ProgressBar mProgressBar;

    private boolean[] mChecked;
    private boolean mFunDataChanged;
    //in order to deal w issue of recycled views causing repeat checkmarks on list, creating
    //member variable to keep track of checked status of a given item, which will be
    //used to set checked status of that particular item

    //mFunDataChanged keeps track of whether items have been added to the main list from the suggestion list


    static SuggestionFragment newInstance(FunType type) {
        SuggestionFragment f = new SuggestionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ChoicesListFragment.CHOICES_TYPE, type);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.suggestion_list_title);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabaseManager = DatabaseManager.get(getActivity());
        mSuggestionFactory = FunSuggestionFactory.get(getActivity());

        mSuggestionSelected = new ArrayList<FunThing>();

        mType = (FunType) getArguments().getSerializable(ChoicesListFragment.CHOICES_TYPE);

        mFunList = mSuggestionFactory.getFunThingSuggestionList(mType);
        mChecked = new boolean[mFunList.size()];

        mArrayAdapter = new CheckedListAdapter(getActivity(), android.R.layout.simple_list_item_multiple_choice, mFunList);
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        mYelpHits = sharedPreferences.getInt(mType.name(), 0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.suggestion_list_fragment, container, false);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progress_suggestion);
        Button doneButton = (Button) v.findViewById(R.id.suggestion_done_button);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChangesToDatabases();
                getActivity().finish();
            }
        });
        Button cancelButton = (Button) v.findViewById(R.id.suggestion_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        mLoadMoreButton = (Button) v.findViewById(R.id.suggestion_load_more_button);
        if (mFunList.size() == 0) mLoadMoreButton.setText(R.string.load_suggestions);
        mLoadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mType == FunType.COOK) {
                    Toast toast = Toast.makeText(getActivity(), "No suggestions available for 'Cook'", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (mType == FunType.EVENT) {
                    Toast toast = Toast.makeText(getActivity(), "Nice try!  Must I do everything for you? Enter your own events!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    getYelpSuggestions();
                }
            }
        });

        ListView listView = (ListView) v.findViewById(R.id.suggestion_list_view);
        View empty = v.findViewById(R.id.suggestion_empty);
        listView.setEmptyView(empty);
        listView.setAdapter(mArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView checkedTextView = ((CheckedTextView) view);
                mChecked[position] = !mChecked[position];
                checkedTextView.setChecked(!checkedTextView.isChecked());
                FunThing funThing = mArrayAdapter.getItem(position);
                if (checkedTextView.isChecked()) {
                    mSuggestionSelected.add(funThing);
                } else {
                    if (mSuggestionSelected.contains(funThing)) {
                        mSuggestionSelected.remove(funThing);
                    }
                }
            }
        });

        return v;
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

    }


    /**
     * sends result telling calling activity if data was changed while the fragment ran
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mFunDataChanged) {
            Intent intent = new Intent(DatabaseManager.ACTION_DB_CHANGED);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            Log.d(TAG, "Broadcast Sent");
        }
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(mType.name(), mYelpHits);
        editor.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * for list of suggestions that have been selected to be transferred into main list,
     * adds to main list and then deletes from suggestions, then notifies adapter of changes
     */
    void saveChangesToDatabases() {
        int listSize = mSuggestionSelected.size();
        if (listSize != 0) {
            for (FunThing funThing : mSuggestionSelected) {
                try {
                    mDatabaseManager.insertFunThing(FunDatabaseHelper.TABLE_FUN, funThing);
                    mDatabaseManager.deleteById(FunDatabaseHelper.TABLE_SUGGESTIONS, funThing.get_id());
                    mSuggestionFactory.deleteSuggestionFromList(funThing.getFunThingType(), funThing);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Insert and/or Save Failed", Toast.LENGTH_LONG).show();
                    listSize--;
                }
            }
            if (listSize > 0 ) {
                mFunDataChanged = true;
                mArrayAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * For a given category, selects and appends appropriate search terms to uri (which by
     * default searches San Francisco and returns 20 objects per load at the moment)
     * Then feeds the resulting uri into a loader to fetch the data
     */
    private void getYelpSuggestions() {

        if (!FunApplication.isDataConnectionAvailable()) {
            Toast toast = Toast.makeText(getActivity(), "Cannot load suggestions unless data connection is available", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {

            String starterURI = "http://api.yelp.com/v2/search?location=San+Francisco&limit=20&sort=2&category_filter=";
            String yelpSearchURI;
            String yelpCat;

            int offset = mYelpHits * 20;
            Log.e(TAG, "offset" + offset);

            switch (mType) {
                case ACTIVITY:
                    yelpCat = YELP_ACTIVITY + ',' + YELP_ENTERTAINMENT;
                    break;
                case BAR:
                    yelpCat = YELP_BARS;
                    break;
                case NIGHTLIFE:
                    yelpCat = YELP_NIGHTLIFE;
                    break;
                case RESTAURANT:
                    yelpCat = YELP_RESTAURANT;
                    break;
                default:
                    yelpCat = "";
                    break;
            }

            yelpSearchURI = starterURI + yelpCat + "&offset=" + offset;

            YelpLoader yelpLoader = new YelpLoader();
            yelpLoader.execute(new String[]{yelpSearchURI});
        }

    }

    /**
     * Takes a uri string as input, performs yelp search based on the string
     * doInBackground returns JSON result
     * onPostExecute parses JSON result, updates the list, and notifies adapters of changes
     */
    private class YelpLoader extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... yelpSearchURL) {
            //obviously better and more detailed exception handing would need to be done in production code
            // OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException, among others

            try {
                String consumerKey = YELP_CONSUMER_KEY;
                String consumerSecret = YELP_CONSUMER_SECRET;
                String token = YELP_TOKEN;
                String tokenSecret = YELP_TOKEN_SECRET;
                String yelpSearchURI = yelpSearchURL[0];
                OAuthConsumer myConsumer = new CommonsHttpOAuthConsumer(consumerKey, consumerSecret);
                myConsumer.setMessageSigner(new HmacSha1MessageSigner());
                myConsumer.setTokenWithSecret(token, tokenSecret);
                myConsumer.setSendEmptyTokens(true);
                myConsumer.setSigningStrategy(new QueryStringSigningStrategy());
                String signedQuery = myConsumer.sign(yelpSearchURI);
                HttpGet request = new HttpGet(signedQuery);
                HttpClient httpClient = new DefaultHttpClient();
                org.apache.http.HttpResponse response =  (org.apache.http.HttpResponse) httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                String jsonResult = EntityUtils.toString(entity);
                return jsonResult;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                mYelpHits++;
                try {
                    JSONObject result = new JSONObject(s);
                    JSONArray businessList = result.getJSONArray("businesses");
                    if (businessList.length() >0) {
                    for (int i = 0; i < businessList.length(); i++) {
                        JSONObject business = businessList.getJSONObject(i);
                        FunThing funSuggestion = new FunThing();
                        funSuggestion.setFunThingType(mType);
                        if (!business.isNull("name")) {
                            String name = business.getString("name");
                            funSuggestion.setFunThingName(name);
                        }
                        if (!business.isNull("mobile_url")) {
                            String url = business.getString("mobile_url");
                            funSuggestion.setFunThingURI(url);
                        }
                        if (!business.isNull("image")) {
                            String image = business.getString("image");
                            funSuggestion.setFunThingPhoto(image);
                        }
                        if (!business.isNull("phone")) {
                            String number = business.getString("phone");
                            funSuggestion.setFunThingPhoneNumber(number);
                        }
                        if (!mFunList.contains(funSuggestion)) {
                            mSuggestionFactory.addSuggestionToList(funSuggestion.getFunThingType(), funSuggestion);
                            funSuggestion.set_id(mDatabaseManager
                                    .insertFunThing(FunDatabaseHelper.TABLE_SUGGESTIONS, funSuggestion));
                        }
                    }
                    } else {
                        Toast toast = Toast.makeText(getActivity(),"There are no more suggestions",Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "something is wrong w json");
                } finally {
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
                if (mArrayAdapter != null) {
                    //don't need to clear or add items to adapter bc is still pointing to same singleton list reference

                    //need to reset size of boolean array to keep track of checkmarks
                    int j = mFunList.size();
                    mChecked = new boolean[j];
                    if (j > 0) mLoadMoreButton.setText(R.string.load_more_suggestions);
                    mArrayAdapter.notifyDataSetChanged();
                }
            } else {
                Toast toast = Toast
                        .makeText(getActivity(), "Uh-oh! Could not download suggestions. Please try again later!",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }


    private class CheckedListAdapter extends ArrayAdapter<FunThing> {

        private final int mResource;
        private final List<FunThing> mObjects;

        private CheckedListAdapter(Context context, int resource, List<FunThing> objects) {
            super(context, resource, objects);
            mResource = resource;
            mObjects = objects;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(mResource, parent, false);
            }
            FunThing funThing = mObjects.get(position);
            CheckedTextView checkedTextView = ((CheckedTextView) convertView);
            checkedTextView.setChecked(mChecked[position]);
            TextView textView = ((TextView) convertView);
            textView.setText(funThing.getFunThingName());

            return convertView;

        }
    }
}
