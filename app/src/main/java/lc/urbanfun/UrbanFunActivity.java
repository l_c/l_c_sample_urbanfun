package lc.urbanfun;

import android.app.Fragment;

public class UrbanFunActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        return UrbanFunFragment.newInstance();
    }

}
