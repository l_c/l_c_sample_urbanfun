package lc.urbanfun;

import android.graphics.Bitmap;

/**
 * Created by lc on 9/17/14.
 */
public interface PhotoFinishedListener {
    void onPhotoFinished(Bitmap bitmap);

    void setPhotoResource(int photoId);
}


