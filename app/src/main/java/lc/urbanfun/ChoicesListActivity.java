package lc.urbanfun;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;

/**
 * Created by lc on 7/23/14.
 * Sets up ViewPager from within which the choiceslistfragment are displayed
 * each fragment contains the list of potential activities for a different category
 */

//ALL FRAGMENTS HERE ARE SUPPORT FRAGMENTS!! (as necessary for ViewPager)


public class ChoicesListActivity extends FragmentActivity {

    public static ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_list);
        PagerTitleStrip titleStrip = (PagerTitleStrip) findViewById(R.id.choices_list_title_strip);
        titleStrip.setTextSpacing(0);
        titleStrip.setBackgroundColor(getResources().getColor(R.color.accent));
        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        //chose fragment pager adapter because right now won't be many categories
        //if add more may need to switch to FragmentStatePagerAdapter
        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                return ChoicesListFragment.newInstance(position);
            }

            @Override
            public int getCount() {
                return FunType.values().length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                FunType funType = FunType.values()[position];
                return funType.getDisplayName();
            }
        });
    }
}
