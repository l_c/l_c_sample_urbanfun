package lc.urbanfun;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by lc on 7/21/14.
 * Launch fragment for application
 * displays buttons backed by list of possible activities, and randomly selects an activity in a given category
 * when the button for that category is pressed, then displays result in ResultsFragment dialog
 * displays welcome message on first time run
 */
public class UrbanFunFragment extends Fragment implements LoaderManager.LoaderCallbacks<Map<FunType, List<FunThing>>> {


    private static final String TAG = "UrbanFunFragment";
    private static final int LOADER_ID = 1;
    private static final String NUMBER_TIMES_RUN = "NumberTimesRun";
    private static Map<FunType, List<FunThing>> mData;
    private final View.OnClickListener gridButtonListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            //be careful here - typecasting the tag so if change tag type in xml, have to change this too
            int position = Integer.parseInt((String) v.getTag());
            FunType type = FunType.values()[position];
            List<FunThing> funList = mData.get(type);
            long funId;
            if (funList.size() > 0) {
                FunThing funThing = randomChooser(funList);
                if (funThing != null) {
                    funId = funThing.get_id();
                } else {
                    funId = -1L;
                }
            } else {
                funId = -1L;
            }
            DialogFragment resultFragment = ResultsFragment.newInstance(type, funId);
            resultFragment.show(getFragmentManager(), "resultdialog");
        }
    };
    private int mHowManyTimesBeenRun;

    public static UrbanFunFragment newInstance() {
        return new UrbanFunFragment();
    }

    /**
     * selects a random item from a passed in list
     *
     * @param choices list of potential items in given category
     */
    private static FunThing randomChooser(List<FunThing> choices) {
        if (choices != null) {
            int numOptions = choices.size();
            if (numOptions != 0) {
                Random random = new Random();
                int choice = random.nextInt(numOptions);
                return choices.get(choice);
            } else {
                return null;
            }
        } else return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getLoaderManager().initLoader(LOADER_ID, null, this);

        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        mHowManyTimesBeenRun = sharedPreferences.getInt(NUMBER_TIMES_RUN, 0);
        mHowManyTimesBeenRun++;

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(NUMBER_TIMES_RUN, mHowManyTimesBeenRun);
        editor.commit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_urban_fun, container, false);

        TableLayout tableLayout = (TableLayout) v.findViewById(R.id.urban_fun_main_table_layout);
        int num = 0;
        for (int i = 0; i < tableLayout.getChildCount(); i++) {
            TableRow tableRow = (TableRow) tableLayout.getChildAt(i);
            for (int j = 0; j < tableRow.getChildCount(); j++) {
                Button button = (Button) tableRow.getChildAt(j);
                button.setText(FunType.values()[num].getDisplayName());
                button.setOnClickListener(gridButtonListener);
                num++;
            }
        }

        if (mHowManyTimesBeenRun == 1) {
            final LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.welcome_view);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayout.setVisibility(View.INVISIBLE);
                }
            });
        }
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_page_options_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add_option:
                addNewFunThing();
                return true;
            case R.id.menu_item_show_list:
                Intent i = new Intent(getActivity(), ChoicesListActivity.class);
                startActivity(i);
                return true;
            case R.id.menu_item_show_archived:
                Intent j = new Intent(getActivity(), ArchivedListActivity.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void addNewFunThing() {
        //take to detail page to create new fun thing
        Intent i = new Intent(getActivity(), FunThingDetailActivity.class);
        i.putExtra(FunThingDetailFragment.FUN_ID, -1L);
        startActivity(i);
    }

    /**
     * Loader to get lists or items for all categories
     *
     * @param id   loader id
     * @param args empty
     * @return Map with category as key and list for that category as value
     */
    @Override
    public Loader<Map<FunType, List<FunThing>>> onCreateLoader(int id, Bundle args) {
        return new FunThingLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Map<FunType, List<FunThing>>> loader, Map<FunType, List<FunThing>> data) {
        if (data != null) {
            mData = data;
        }
    }

    @Override
    public void onLoaderReset(Loader<Map<FunType, List<FunThing>>> loader) {
        //nothing critical needed at the moment
    }
}

