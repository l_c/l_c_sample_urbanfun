package lc.urbanfun;

import android.app.Fragment;

/**
 * Created by lc on 8/11/14.
 */

public class SuggestionListActivity extends SingleFragmentActivity {

    private static final String TYPE = "type";


    @Override
    protected Fragment createFragment() {
        FunType type = (FunType) getIntent().getSerializableExtra(ChoicesListFragment.CHOICES_TYPE);
        return SuggestionFragment.newInstance(type);
    }

}