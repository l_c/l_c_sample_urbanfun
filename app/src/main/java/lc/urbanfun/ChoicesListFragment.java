package lc.urbanfun;


import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by lc on 7/23/14.
 * Have to use support (v4) fragment in this bc viewpager is only in the support package
 * Each page displays the current list of possible activities in a given category
 * Includes options to import suggestion into list, as well as add, delete, or archive items
 * Also receives broadcast from CompletedFragment so knows when that fragment has changed data
 */

public class ChoicesListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Map<FunType, List<FunThing>>> {

    public static final String CHOICES_TYPE = "Type_of_Funthing";
    private static final String PAGER_POSITION = "position";
    private static final int LOADER_ID = 1;
    private static final String TAG = "ChoicesListFragment";


    private DatabaseManager mDatabaseManager;
    private FunAdapter mAdapter;
    private android.app.FragmentManager mFragmentManager;

    private FunType mType;
    private boolean mDataChanged;
    // mDataChanged is to keep track of changes that occur within the fragment itself so that broadcast
    // announcing changes can be made when fragment is destroyed if data was in fact changed

    private final BroadcastReceiver dbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getLoaderManager().getLoader(LOADER_ID).forceLoad();
        }
    };

    /**
     * @param position tells the viewpager which page to start on, so if user navigates away will
     *                 return to same position in pager
     * @return the correct listfragment for the category associated with the page being generated
     */
    public static ChoicesListFragment newInstance(int position) {
        ChoicesListFragment choicesListFragment = new ChoicesListFragment();
        Bundle args = new Bundle();
        args.putInt(PAGER_POSITION, position);
        choicesListFragment.setArguments(args);
        return choicesListFragment;
    }

    /**
     * method gets the list of objects to be displayed from FunThingLoaderV4
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //might wanna move this to where action bar definitely created
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().setTitle(R.string.choice_list_title);
        int position = getArguments().getInt(PAGER_POSITION);
        mType = FunType.values()[position];

        mDatabaseManager = DatabaseManager.get(getActivity());
        mFragmentManager = getActivity().getFragmentManager();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        mAdapter = new FunAdapter();
        setListAdapter(mAdapter);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEmptyText(getString(R.string.choices_list_empty_text));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.choices_list_options_menu, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        //broadcast manager is set to receive notification when other fragments make changes
        //that affect the dataset
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .registerReceiver(dbReceiver, new IntentFilter(DatabaseManager.ACTION_DB_CHANGED));
    }



    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(dbReceiver);
    }

    /**
     * sends broadcast if changes to data occurred withing the fragment
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mDataChanged) {
            Intent intent = new Intent(DatabaseManager.ACTION_DB_CHANGED);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
            case R.id.choices_list_suggestions:
                openSuggestionListFragment();
                return true;
            case R.id.choices_list_add:
                openFunDetailFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts FunThingDetailFragment, passing in the id of the selected item and the current
     * position of the viewpager.
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent i = new Intent(getActivity(), FunThingDetailActivity.class);
        FunThing funThing = (FunThing) getListAdapter().getItem(position);
        long funId = funThing.get_id();
        int viewpagerPosition = ChoicesListActivity.mViewPager.getCurrentItem();
        i.putExtra(FunThingDetailFragment.FUN_ID, funId);
        i.putExtra(FunThingDetailFragment.PAGER_POSITION, viewpagerPosition);
        startActivity(i);
    }





    private void openSuggestionListFragment() {
        Intent i = new Intent(getActivity(), SuggestionListActivity.class);
        int j = ChoicesListActivity.mViewPager.getCurrentItem();
        FunType type = FunType.values()[j];
        i.putExtra(CHOICES_TYPE, type);
        startActivity(i);
    }

    private void openFunDetailFragment() {
        Intent k = new Intent(getActivity(), FunThingDetailActivity.class);
        int viewpagerPosition = ChoicesListActivity.mViewPager.getCurrentItem();
        k.putExtra(FunThingDetailFragment.PAGER_POSITION, viewpagerPosition);
        startActivity(k);
    }

    @Override
    public Loader<Map<FunType, List<FunThing>>> onCreateLoader(int id, Bundle args) {
        return new FunThingLoaderV4(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Map<FunType, List<FunThing>>> loader, Map<FunType, List<FunThing>> data) {
        List<FunThing> checkList = data.get(mType);
        if (checkList != null) {
            mAdapter.clear();
            mAdapter.addAll(checkList);
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onLoaderReset(Loader<Map<FunType, List<FunThing>>> loader) {
        //nothing critical needed at the moment
    }



    private class FunAdapter extends ArrayAdapter<FunThing> {

        public FunAdapter() {
            super(getActivity(), 0);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.fragment_choices_list_item_view, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.funName = (TextView) convertView.findViewById(R.id.choices_list_item_textview);
                viewHolder.deleteButton = (ImageButton) convertView.findViewById(R.id.choices_list_item_delete);
                viewHolder.completedButton = (ImageButton) convertView.findViewById(R.id.choices_list_item_done);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final FunThing funThing = getItem(position);

            viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = mDatabaseManager.deleteById(FunDatabaseHelper.TABLE_FUN, funThing.get_id());
                    mDataChanged = true;
                    mAdapter.remove(funThing);
                    mAdapter.notifyDataSetChanged();
                }
            });

            viewHolder.completedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment completedFragment = CompletedFragment.newInstance(funThing.get_id());
                    mFragmentManager.beginTransaction()
                            .add(completedFragment, "completed fragment")
                            .commit();
                }
            });

            viewHolder.funName.setText(funThing.getFunThingName());
            return convertView;
        }

        private class ViewHolder {
            TextView funName;
            ImageButton deleteButton;
            ImageButton completedButton;
        }


    }
}