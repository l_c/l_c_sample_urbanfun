package lc.urbanfun;

import android.app.Fragment;

/**
 * Created by lc on 8/1/14.
 */
public class FunThingDetailActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        //id will be -1 if it is a new item that has not previously been created
        //or the id of the selected funthing if it is displaying details of previously created item
        long id = getIntent().getLongExtra(FunThingDetailFragment.FUN_ID, -1L);
        //non-zero pager position means detail was requested from certain page within the viewpager
        //so when return to viewpager should return to that page instead of the first page
        int pagerPosition = 0;
        if (getIntent().hasExtra(FunThingDetailFragment.PAGER_POSITION)) {
            pagerPosition = getIntent().getIntExtra(FunThingDetailFragment.PAGER_POSITION, 0);
        }
        //data changed variable was added to make sure ui is still updated immediately in the case that
        // multiple items are added in a row (and thus multiple detail fragments are created, with the
        // default mDataAdded being false) - without this if many are saved but the last one
        // is not saved, the choiceslistfragment ui wouldn't immediately reflect the ones that had
        // been successfully added

        boolean dataChanged = false;
        if (getIntent().hasCategory(FunThingDetailFragment.DATA_CHANGED)) {
            dataChanged = getIntent().getBooleanExtra(FunThingDetailFragment.DATA_CHANGED, false);
        }
        return FunThingDetailFragment.newInstance(id, pagerPosition, dataChanged);
    }
}
