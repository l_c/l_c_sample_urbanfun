package lc.urbanfun;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

/**
 * Created by lc on 6/24/14.
 * template for activities which will only support one fragment within a simple frame layout
 */
public abstract class SingleFragmentActivity extends Activity {

    protected abstract Fragment createFragment();


    private static int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    private static int getFragmentContainerIdFromLayout() {
        return R.id.fragmentContainer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(getFragmentContainerIdFromLayout());

        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction().add(getFragmentContainerIdFromLayout(), fragment).commit();
        }
    }


}


