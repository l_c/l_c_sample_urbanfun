package lc.urbanfun;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import lc.urbanfun.DatabaseManager;
import lc.urbanfun.FunDatabaseHelper;
import lc.urbanfun.FunThing;
import lc.urbanfun.FunType;

/**
 * Created by lc on 8/27/14.
 * Just straight out copied this from FunThingLoader and changed it to extend v4 asynctaskloader
 * Couldn't find a way to have same loader for normal and v4 fragments
 * Can take away unneeded code in this later if stay with this fix
 */
public class FunThingLoaderV4 extends AsyncTaskLoader<Map<FunType, List<FunThing>>> {

    private static final String TAG = "FunThingLoaderV4";

    private static Context mAppContext;
    private static Map<FunType, List<FunThing>> mFunMap;

    public FunThingLoaderV4(Context context) {
        super(context);
        mAppContext = context.getApplicationContext();
    }

    @Override
    public Map<FunType, List<FunThing>> loadInBackground() {
        Map<FunType, List<FunThing>> funThingMap = new EnumMap<FunType, List<FunThing>>(FunType.class);
        for (FunType type : FunType.values()) {
            try {
            List<FunThing> funThingList = new ArrayList<FunThing>();
            DatabaseManager databaseManager = DatabaseManager.get(mAppContext);
            DatabaseManager.FunCursor funCursor = databaseManager.queryFunType(FunDatabaseHelper.TABLE_FUN, type);
            if (funCursor.moveToFirst()) {
                while (!funCursor.isAfterLast()) {
                    funThingList.add(funCursor.getFunThing());
                    funCursor.moveToNext();
                }
            } else {
                Log.d(TAG, "Didn't find any" + type + "results in database");
                //fun thing list will be null
            }
            funCursor.close();
            funThingMap.put(type, funThingList); }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return funThingMap;
    }


    @Override
    public void deliverResult(Map<FunType, List<FunThing>> data) {
        //since not using cursor or anything at the moment, not protecting old data before delivering new

        if (isReset()) {
            return;
        }

        mFunMap = data;

        if (isStarted())
            super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {

        LocalBroadcastManager.getInstance(mAppContext)
                .registerReceiver(dbReceiver, new IntentFilter(DatabaseManager.ACTION_DB_CHANGED));

        if (mFunMap != null) {
            deliverResult(mFunMap);
        }
        if (takeContentChanged() || mFunMap == null) {
            forceLoad();
        }
    }


    @Override
    protected void onStopLoading() {
        //attempt to cancel the current load task if possible
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        //ensure loading has stopped
        onStopLoading();
        if (mFunMap != null) mFunMap = null;
        //keeping receiver for now
    }

    private final BroadcastReceiver dbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onContentChanged();
        }
    };
}
