package lc.urbanfun;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by lc on 9/17/14.
 * moves photofetching to centralized utility so can call from more than one fragment
 * takes a given item and finds or fetches via async task photo for that item, then
 * notifies returns resource for photo via attached listener
 */
public class PhotoFetcher extends ContextWrapper {

    private static final String PACKAGE_NAME = PhotoFetcher.class.getPackage().getName();
    private static final String TAG = "PhotoFetcher";


    private PhotoFetcher(Context base) {
        super(base);
    }

    /**
     * called if photo has string resource describing drawable resource
     * goes from string identifying named resource to actual int of that id
     */
    public static int getResourceId(String stringPhoto) {
        //goes from string representing the int identifier for an item in resources to the corresponding int
        int i = -1;
        try {
            i = FunApplication.getResourcesStatic().getIdentifier(stringPhoto, "drawable", PACKAGE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    /**
     * finds appropriate assigned or default photo for item passed in
     * if item has photo at a web address,calls asynctask method to retrieve it
     *
     * @param funResult item for which photo is being found
     * @param pListener listener in calling class to allow it to receive result
     */
    public static void setPhoto(FunThing funResult, PhotoFinishedListener pListener) {
        String photo = funResult.getFunThingPhoto();
        FunType type = funResult.getFunThingType();

        //finds default photo for item of the given type
        int photoId = type.getPhotoId();


        //some web pictures will not obey these rules, but for now, the default will be good enough

        //if item contains a photo resource, either through a weblink or on the device (currently only in drawable folder)
        //get that photo resource, if not, or can't find, just set it to default photo for that category
        if (photo != null) {
            if ((photo.contains(".com")) && FunApplication.isDataConnectionAvailable()) {
                //fetch from web
                if (!photo.contains("http")) photo = "http://" + photo;
                LoadPicture loadPicture = new LoadPicture(pListener);
                loadPicture.execute(new String[]{photo});
            } else {
                int i = PhotoFetcher.getResourceId(photo);
                if (i > 0) {
                    pListener.setPhotoResource(i);
                } else {
                    pListener.setPhotoResource(photoId);
                }
            }
        } else {
            pListener.setPhotoResource(photoId);
        }
    }

    /**
     * gets picture from web
     */
    private static class LoadPicture extends AsyncTask<String, Void, Bitmap> {
        HttpURLConnection connection;

        private final PhotoFinishedListener photoListener;

        public LoadPicture(PhotoFinishedListener pListener) {
            this.photoListener = pListener;
        }

        @Override
        protected Bitmap doInBackground(String... imageURL) {
            //android.os.Debug.waitForDebugger();
            String urlString = imageURL[0];
            try {
                URL url = new URL(urlString);
                connection = (HttpURLConnection) url.openConnection();
            } catch (MalformedURLException e) {
                Log.d(TAG, "url exception");
                return null;
            } catch (IOException e) {
                Log.d(TAG, "io exception");
                return null;
            }

            try {

                InputStream in = connection.getInputStream();
                Log.d(TAG, "got past streams");
                int i = connection.getResponseCode();
                Log.d(TAG, "Response code " + i);
                if (i != HttpURLConnection.HTTP_OK) {
                    return null;
                }
                return BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                connection.disconnect();
            }

        }

        @Override
        protected void onPostExecute(Bitmap bmp) {
            photoListener.onPhotoFinished(bmp);
        }
    }

}
