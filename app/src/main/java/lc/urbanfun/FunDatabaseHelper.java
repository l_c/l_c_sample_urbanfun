package lc.urbanfun;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lc on 8/2/14.
 * Do not access these methods or the constructor directly
 * Use Database manager to control flow to the database
 */
public class FunDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "FunDatabaseHelper";
    private static final String DB_NAME = "fun.sqlite";
    private static final int VERSION = 1;
    public static final String TABLE_ARCHIVE = "fun_archive";
    public static final String TABLE_FUN = "fun_thing";
    public static final String TABLE_SUGGESTIONS = "fun_suggestion";
    public static final String COLUMN_FUN_TYPE = "fun_type";
    public static final String COLUMN_FUN_NAME = "fun_name";
    public static final String COLUMN_FUN_LOCATION = "fun_location";
    public static final String COLUMN_FUN_PHONE_NUMBER = "fun_phone";
    public static final String COLUMN_FUN_DATE = "fun_date";
    public static final String COLUMN_FUN_URI = "fun_Uri";
    public static final String COLUMN_FUN_PHOTO = "fun_photo";
    public static final String COLUMN_FUN_ID = "_id";
    public static final String COLUMN_FUN_STARS = "fun_stars";
    public static final String COLUMN_FUN_SUGGESTER = "fun_suggester";

    /*fun_suggester currently only exists in fun_suggestion table
    keeping it around as a reminder (for when want to include suggestions from people)
    for reusability of code if start using it might put the column in other
    databases as well and just have it set to null*/

    public FunDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table fun_thing (" +
                "_id integer primary key autoincrement, fun_type string, fun_name string," +
                " fun_location string, fun_phone string, fun_date string, fun_Uri string," +
                " fun_photo string, fun_stars int)");

        db.execSQL("create table fun_archive (" +
                "_id integer primary key autoincrement, fun_type string, fun_name string," +
                " fun_location string, fun_phone string, fun_date string, fun_Uri string," +
                " fun_photo int, fun_stars int)");

        db.execSQL("create table fun_suggestion (" +
                "_id integer primary key autoincrement, fun_type string, fun_name string," +
                " fun_location string, fun_phone string, fun_date string, fun_Uri string," +
                " fun_photo int, fun_stars int,fun_suggester string)");

        //starter list is so that when the app is first loaded there will be some sample activities
        //for people to play with to see how it works
        createStarterList(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //nothing yet
    }


    private static void createStarterList(SQLiteDatabase db) {

        //for demonstration purposes only - not trying to be overly efficient
        //obviously could have made a method to avoid the repeated for loops

        final String[] activityNames = {"Oysters at Tomales Bay", "Escape Room", "Kayak to Angel Island",
                "Bike to Sausalito", "Treasure Island Flea Market"};
        final String[] activityPhoto = {"oysters", "escape", "angel", "bike", "treasure"};

        final String[] cookNames = {"Marinated Flank Steak", "Salmon with Tomatoes",
                "Black Cod with Miso"};
        final String[] cookWebsite = {"http://allrecipes.com/Recipe/Marinated-Flank-Steak/Detail.aspx",
                "http://allrecipes.com/Recipe/Salmon-with-Tomatoes/Detail.aspx",
                "http://www.foodandwine.com/recipes/black-cod-with-miso"};
        final String[] cookPhoto = {"http://images.media-allrecipes.com/userphotos/250x250/00/00/87/8707.jpg",
                "http://images.media-allrecipes.com/userphotos/250x250/00/03/89/38988.jpg",
                "www.foodandwine.com/images/sys/201009-r-black-cod.jpg"};

        final String[] eventNames = {"Treasure Island Music Festival", "Hardly Strictly Bluegrass",
                "SF Giants Game"};
        final String[] eventWebsite = {"http://treasureislandfestival.com/2014/",
                "http://www.hardlystrictlybluegrass.com/2014/",
                "http://sanfrancisco.giants.mlb.com/index.jsp?c_id=sf"};
        final String[] eventPhoto = {"treasure_island", "hardlystrictly", "sfgiants"};

        final String[] barNames = {"Trick Dog", "Hard Water", "Bluxome Street Winery", "Shotwell's",
                "Hotel Biron"};
        final String[] barPhone = {"(415) 471-2999", "(415) 392-3021", "(415) 543-5353", "(415) 648-4104",
                "(415) 703-0403"};
        final String[] barPhoto = {"trick_dog", "hard_water", "bluxome_winery", "shotwells", "biron"};

        final String[] restaurantNames = {"Gary Danko", "La Ciccia", "Cotogna", "Mission Chinese"};
        final String[] restaurantPhone = {"(415) 749-2060", "(415) 550-8114", "(415) 775-8508",
                "(415) 863-2800"};
        final String[] restaurantPhoto = {"gary_danko", "laciccia", "cotogna", "mission_chinese"};
        final String[] restaurantWebsite = {"http://www.garydanko.com/site/forms/reservations.html",
                "http://www.opentable.com/la-ciccia", "http://www.opentable.com/cotogna", null};

        final String[] nightlifeNames = {"Ruby Skye", "Twang", "DNA Lounge"};
        final String[] nightlifePhoto = {"ruby_skye", "twang", "dna_lounge"};

        for (int i = 0; i < barNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, barNames[i]);
            cv.put(COLUMN_FUN_PHOTO, barPhoto[i]);
            cv.put(COLUMN_FUN_PHONE_NUMBER, barPhone[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.BAR.name());
            db.insert(TABLE_FUN, null, cv);
        }

        for (int i = 0; i < restaurantNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, restaurantNames[i]);
            cv.put(COLUMN_FUN_PHOTO, restaurantPhoto[i]);
            cv.put(COLUMN_FUN_PHONE_NUMBER, restaurantPhone[i]);
            cv.put(COLUMN_FUN_URI, restaurantWebsite[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.RESTAURANT.name());
            db.insert(TABLE_FUN, null, cv);
        }

        for (int i = 0; i < nightlifeNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, nightlifeNames[i]);
            cv.put(COLUMN_FUN_PHOTO, nightlifePhoto[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.NIGHTLIFE.name());
            db.insert(TABLE_FUN, null, cv);
        }

        for (int i = 0; i < cookNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, cookNames[i]);
            cv.put(COLUMN_FUN_PHOTO, cookPhoto[i]);
            cv.put(COLUMN_FUN_URI, cookWebsite[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.COOK.name());
            db.insert(TABLE_FUN, null, cv);
        }

        for (int i = 0; i < eventNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, eventNames[i]);
            cv.put(COLUMN_FUN_PHOTO, eventPhoto[i]);
            cv.put(COLUMN_FUN_URI, eventWebsite[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.EVENT.name());
            db.insert(TABLE_FUN, null, cv);
        }

        for (int i = 0; i < activityNames.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FUN_NAME, activityNames[i]);
            cv.put(COLUMN_FUN_PHOTO, activityPhoto[i]);
            cv.put(COLUMN_FUN_TYPE, FunType.ACTIVITY.name());
            db.insert(TABLE_FUN, null, cv);
        }
    }
}

