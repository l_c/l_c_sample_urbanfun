package lc.urbanfun;

import lc.urbanfun.R;

/**
 * Created by lc on 9/01/14.
 * yay for enums!
 * this enum is for the categories of funThing used within the app
 * currently contains a photo and a displayname (so can display name not in all-caps)
 */


public enum FunType {
    BAR(R.drawable.bar_fun, "Bar"),
    RESTAURANT(R.drawable.restaurant_fun, "Restaurant"),
    ACTIVITY(R.drawable.activity_fun, "Activity"),
    EVENT(R.drawable.event_fun, "Event"),
    COOK(R.drawable.cook_fun, "Cook"),
    NIGHTLIFE(R.drawable.nightlife_fun, "Nightlife");

    private final int photoId;
    private final String displayName;

    private FunType(int photoId, String displayName) {

        this.photoId = photoId;
        this.displayName = displayName;
    }

    public int getPhotoId() {
        return photoId;
    }

    public String getDisplayName() {
        return displayName;
    }
}

