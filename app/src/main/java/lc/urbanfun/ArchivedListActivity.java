package lc.urbanfun;

import android.app.Fragment;

/**
 * Created by lc on 8/22/14.
 */
public class ArchivedListActivity extends SingleFragmentActivity {

    //as a pattern implementing all fragments through newInstance for easier extensibility later
    @Override
    protected Fragment createFragment() {
        return ArchivedListFragment.newInstance();
    }
}
