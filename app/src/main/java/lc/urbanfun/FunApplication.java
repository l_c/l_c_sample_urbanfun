package lc.urbanfun;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by lc on 9/17/14.
 * recently implemented to share a couple immutable things to give opportunity to do things
 * in a static context elsewhwere
 * right not only need resources but could also return static appcontext, packagename, etc.
 */
public class FunApplication extends Application {
    private static Context sContext;

    public static Resources getResourcesStatic() {
        return FunApplication.sContext.getResources();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    //quick fix for now, would throw this into a separate utility class
    public static boolean isDataConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) sContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting());
    }
}

